/*
 * Omnivox-Scraper
 * Copyright (c) 2021-2021 solonovamax <solonovamax@12oclockpoint.com>
 *
 * The file build.gradle.kts is part of Omnivox-Scraper
 * Last modified on 07-09-2021 02:29 p.m.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * OMNIVOX-SCRAPER IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.konan.properties.Properties

plugins {
    java
    signing
    `java-library`
    `maven-publish`
    kotlin("jvm") version "1.5.21"
    id("org.jetbrains.dokka") version "1.5.0"
    id("org.ajoberstar.grgit") version "4.0.2"
}

group = "com.solostudios.omnivoxscraper"
//version = "0.0.1-alpha.3"
val versionObj = Version("0", "0", "1")
version = versionObj

repositories {
    mavenCentral()
}

dependencies {
    implementation("it.skrape:skrapeit:1.1.5")
    
    implementation("org.mnode.ical4j:ical4j:3.1.0")
    implementation("net.sf.biweekly:biweekly:0.6.6")
    implementation("org.jetbrains:annotations:22.0.0")
    implementation("org.slf4j:slf4j-api:1.7.32")
    
    implementation("com.fasterxml.jackson.core:jackson-core:2.12.5")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.12.5")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.12.5")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.12.5")
    
    //    testImplementation("junit:junit:4.13")
    //runtime implementation of slf4j
    testRuntimeOnly("ch.qos.logback:logback-classic:1.2.5")
    //testing
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.2")
    
    //implementation("org.jsoup:jsoup:1.13.1")
}

tasks.test {
    useJUnitPlatform()
    
    systemProperty("OMNIVOX_PASSWORD", getSecret("omnivox_password"))
    systemProperty("OMNIVOX_USERNAME", getSecret("omnivox_username"))
    
    maxHeapSize = "1G"
    ignoreFailures = false
    failFast = true
    maxParallelForks = 12
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val dokkaHtml by tasks.getting(DokkaTask::class)

val javadoc by tasks.getting(Javadoc::class)

val jar by tasks.getting(Jar::class)

val javadocJar by tasks.creating(Jar::class) {
    dependsOn(dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaHtml.outputDirectory)
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets["main"].allSource)
}

tasks.withType<Jar> {
    group = "Jar"
    manifest {
        attributes("Name" to "Omnivox Scrapper",
                   "Specification-Title" to "Omnivox Scrapper",
                   "Specification-Version" to project.version,
                   "Specification-Vendor" to "Solo Studios",
                   "Built-By" to System.getProperty("user.name"),
                   "Build-Jdk" to System.getProperty("java.version"),
                   "Automatic-Module-Name" to "ca.solo-studios.omnivoxscrapper")
        
    }
}


publishing {
    publications {
        create<MavenPublication>("maven") {
            artifact(sourcesJar)
            artifact(javadocJar)
            artifact(jar)
            
            version = version as String
            groupId = group as String
            artifactId = "omnivox-scraper"
            
            pom {
                name.set("Omnivox Scraper")
                description.set("A scraper for the shitty omnivox website")
                url.set("https://gitlab.com/solonovamax/OmnivoxScraper")
                
                inceptionYear.set("2021")
                
                licenses {
                    license {
                        name.set("MIT License")
                        url.set("https://mit-license.org/")
                    }
                }
                developers {
                    developer {
                        id.set("solonovamax")
                        name.set("solonovamax")
                        email.set("solonovamax@12oclockpoint.com")
                        url.set("https://gitlab.com/solonovamax")
                    }
                }
                issueManagement {
                    system.set("GitHub")
                    url.set("https://gitlab.com/solonovamax/OmnivoxScraper/issues")
                }
                scm {
                    connection.set("scm:git:https://github.com/solonovamax/OmnivoxScraper.git")
                    developerConnection.set("scm:git:ssh://gitlab.com/solonovamax/OmnivoxScraper.git")
                    url.set("https://gitlab.com/solonovamax/OmnivoxScraper/")
                }
            }
        }
    }
    
    repositories {
        maven {
            name = "sonatypeStaging"
            url = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2")
            credentials(org.gradle.api.credentials.PasswordCredentials::class)
        }
        maven {
            name = "sonatypeSnapshot"
            url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            credentials(PasswordCredentials::class)
        }
    }
}

signing {
    useGpgCmd()
    sign(publishing.publications["maven"])
}


fun getSecret(key: String): Any {
    val props = Properties()
    props.load(file("secrets.properties").inputStream())
    return props[key]!!
}

/**
 * Version class that does version stuff.
 */
@Suppress("MemberVisibilityCanBePrivate")
class Version(val major: String, val minor: String, val patch: String) {
    val localBuild: Boolean
        get() = env["BUILD_NUMBER"] == null
    
    val buildNumber: String
        get() = env["BUILD_NUMBER"] ?: "0"
    
    val gitHash: String
        get() = grgit.head().id
    
    val shortGitHash: String
        get() = grgit.head().id
    
    override fun toString(): String {
        return if (localBuild) // Only use git hash if it's a local build.
            "$major.$minor.$patch-local+${getGitHash()}"
        else
            "$major.$minor.$patch+${env["BUILD_NUMBER"]}"
    }
}

fun getGitHash(): String = grgit.head().abbreviatedId

val env: Map<String, String>
    get() = System.getenv()
